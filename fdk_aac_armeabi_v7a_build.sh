# Created by jianxi on 2017/6/4
# https://github.com/mabeijianxi
# mabeijianxi@gmail.com
# /home/ubuntu/android-ndk-r21d/toolchains/llvm/prebuilt/linux-x86_64/bin

NDK_HOME=/home/ubuntu/android-ndk-r21d
ANDROID_API=android-21
API=21

SYSROOT=$NDK_HOME/toolchains/llvm/prebuilt/linux-x86_64/sysroot/

ANDROID_BIN=$NDK_HOME/toolchains/llvm/prebuilt/linux-x86_64/bin

CROSS_COMPILE=${ANDROID_BIN}/arm-linux-androideabi

CC_CROSS_COMPILE=${ANDROID_BIN}/armv7a-linux-androideabi

basepath=$(cd `dirname $0`; pwd)

echo "$basepath"


CPU=armeabi-v7a


CFLAGS=" "

FLAGS="--enable-static  --host=armv7a-linux-androideabi --target=android  --disable-asm"

export CXX="${CC_CROSS_COMPILE}${API}-clang++ --sysroot=${SYSROOT}"

export LDFLAGS=" -L$SYSROOT/usr/lib  $CFLAGS "

export CXXFLAGS=$CFLAGS

export CFLAGS=$CFLAGS

export CC="${CC_CROSS_COMPILE}${API}-clang --sysroot=${SYSROOT}"

export AR="${CROSS_COMPILE}-ar"

export LD="${CROSS_COMPILE}-ld"

export AS="${CROSS_COMPILE}-as"

echo "-----------------------------"
echo $CC

./configure $FLAGS \
--enable-pic \
--enable-strip \
--prefix=${basepath}/android/$CPU

make clean
make
make install
